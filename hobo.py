#!/usr/bin/env python3

"""
Collection of tools for organizing bibliographies as file system databases
"""

import argparse
import sys

import labels
import merge
import extract
import unify


HELP_LABELS = {
    'top'    : "search tex files for cited labels and print them",
    'inputs' : ".tex file or a ditectory to be searched for tex files. If " +
               "not specified, current working directory will be searched.",
}

HELP_MERGE = {
    'top' : "merge bib files into a database",
    '-i'  : "input files",
    '-d'  : "output database directory",
    '-p'  : "create database directory and its parents",
    '-f'  : "replace all existing entries",
    '-l'  : "file with a Python function 'labelfunc' that generates a label " +
            "from a BibTex entry",
}

HELP_EXTRACT = {
    'top' : "extract entries from databases into a bib file",
    '-d'  : "database directories to search; if not specified, current " +
            "directory will be searched",
    '--surround-str'  : "strings surrounding an entry; default is { }",
    '--only-fields'   : "extract only these fields",
    '--except-fields' : "extract all fields except of these",
    '--abbrev-first'  : "abbrevite first names",
}

HELP_UNIFY = {
    'top' : "convert any string to a unified format",
    '-i'  : "input files with strings; each string on its own line",
}


def add_parser_labels(subparsers):
    parser = subparsers.add_parser(
        'labels',
        description=labels.__doc__,
        help=HELP_LABELS['top'])
    parser.add_argument(
        'inputs',
        nargs='*',
        metavar='INP',
        help=HELP_LABELS['inputs'])
    parser.set_defaults(func=labels.main)


def add_parser_merge(subparsers):
    parser = subparsers.add_parser(
        'merge',
        description=merge.__doc__,
        help=HELP_MERGE['top'])
    parser.add_argument(
        '-i',
        required=False,
        nargs='+',
        default=[],
        help=HELP_MERGE['-i'])
    parser.add_argument(
        '-d',
        required=True,
        nargs=1,
        help=HELP_MERGE['-d'])
    parser.add_argument(
        '-p', '--parents',
        action='store_true',
        default=False,
        help=HELP_MERGE['-p'])
    parser.add_argument(
        '-f', '--force',
        action='store_true',
        help=HELP_MERGE['-f'])
    parser.add_argument(
        '-l', '--labelfunc',
        required=False,
        help=HELP_MERGE['-l'])
    parser.set_defaults(func=merge.main)


def add_parser_extract(subparsers):
    parser = subparsers.add_parser(
        'extract',
        description=extract.__doc__,
        help=HELP_EXTRACT['top'])
    parser.add_argument(
        '-d',
        required=False,
        nargs='+',
        help=HELP_EXTRACT['-d'])
    parser.add_argument(
        '--surround-str',
        required=False,
        nargs=2,
        type=str,
        default=['{', '}'],
        metavar=('S1', 'S2'),
        help=HELP_EXTRACT['--surround-str'])
    fields_group = parser.add_mutually_exclusive_group()
    fields_group.add_argument(
        '--only-fields',
        required=False,
        nargs='+',
        type=str,
        metavar='FIELD',
        help=HELP_EXTRACT['--only-fields'])
    fields_group.add_argument(
        '--except-fields',
        required=False,
        nargs='+',
        type=str,
        metavar='FIELD',
        help=HELP_EXTRACT['--except-fields'])
    parser.add_argument(
        '--abbrev-first',
        action='store_true',
        help=HELP_EXTRACT['--abbrev-first'])
    parser.set_defaults(func=extract.main)


def add_parser_unify(subparsers):
    parser = subparsers.add_parser(
        'unify',
        description=unify.__doc__,
        help=HELP_UNIFY['top'])
    parser.add_argument(
        '-i',
        required=False,
        nargs='+',
        default=[],
        help=HELP_UNIFY['-i'])
    parser.set_defaults(func=unify.main)


def main():
    parser = argparse.ArgumentParser(description=__doc__, prog="hobo")
    subparsers = parser.add_subparsers(title="subcommands")

    add_parser_labels(subparsers)
    add_parser_merge(subparsers)
    add_parser_extract(subparsers)
    add_parser_unify(subparsers)

    args = parser.parse_args()
    return args.func(args)


if __name__ == "__main__":
    sys.exit(main())
