"""
Utilities used by more scripts.
"""

import os
import select
import sys

from pathlib import Path
from urllib.error import HTTPError, URLError
from urllib.parse import urlparse
from urllib.request import urlopen


class UrlFileOpenError(Exception):
    pass


def err(*args, **kwargs):
    print("Error:", *args, file=sys.stderr, **kwargs)
    return 1


def read_stdin():
    stdin_lines = []
    while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = sys.stdin.readline()
        if line:
            stdin_lines.append(line)
        else:
            break
    return '\n'.join(stdin_lines).strip()


def read_from(inp):
    """Read from an input 'inp' into a string.

    'inp' can be either
        * a file
        * URL

    Args:
        inp: file path, URL or sys.stdin

    Raises:
        UrlFileOpenError: When
            a) HTTPError, URLError
            or
            b) IsADirectoryError, FileNotFoundError
            happens during opening URL/file

    Returns:
        file_str (str): File/URL contents as a UTF-8 encoded string
    """

    try:
        return open(inp, 'r').read()
    except IsADirectoryError:
        raise UrlFileOpenError("'{}' is a directory.".format(inp))
    except FileNotFoundError:
        inp = str(inp)
        parsedurl = urlparse(inp)
        if parsedurl.scheme:
            try:
                return urlopen(inp).read().decode('utf-8')
            except (HTTPError, URLError):
                raise UrlFileOpenError("Can't open URL '{}'".format(inp))
        else:
            raise UrlFileOpenError("Can't open file '{}'.".format(inp))
