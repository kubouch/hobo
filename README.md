# hobo - Homemade bibliography organizer

Assortment of scripts and examples for managing a simple file-based
bibliography database.


## Installation

Install requirements
```
pip install --user bibtexparser
```

Clone the repository
```
git clone https://gitlab.com/kubouch/hobo.git
cd hobo
```

Add `hobo.py` to the PATH, for example
```
ln -rs hobo.py ~/.local/bin/hobo
```


## Bibtex format

* https://bibtexparser.readthedocs.io/en/master/bibtex_conv.html
* http://www.bibtex.org/Format/

Following terminology is used:
```
@ENTRYTYPE{ID,
  field_name = {field_value},
...
  field_name = {field_value}
}
```

The `ID` is also called `label`.


## Examples

### `hobo merge`

#### Custom label generation

The `-l`, `--labelfunc` command-line option can be used to provide a file with
a Python function for generating custom labels from BibTex entries.
There are several conditions:
* The file has to implement a function called `labelfunc`
* `labelfunc` must accept 1 argument - the BibTex entry (usually obtained with
  bibtexparser)
* `labelfunc` must return a str with the generated label
The name/location of the file doesn't matter but it should be readable.

The following example of 'labelfunc' generates a label in a 'surnameyear'
format (e.g. newton1704):
```python
def labelfunc(entry):
    try:
        year = entry['year']
    except KeyError:
        # field 'year' not present
        year = "9999"

    try:
        first_author = entry['author'].strip('"{}"').split('and')[0].strip()
    except KeyError:
        # field 'author' not present
        first_author = "Unknown Unknown"

    if ',' in first_author:
        # name is in a "Surname, N." format
        first_author_surname = first_author.split(',')[0]
    else:
        # name is in a "Name Surname" format
        first_author_surname = first_author.split()[-1]
    first_author_surname = first_author_surname.strip('"{}"')

    return first_author_surname.lower() + year
```
Save it to e.g. `~/.config/hobo/labelfunc.py` and use by calling
`hobo merge ... -l ~/.config/hobo/labelfunc.py`.

### `hobo unify`

```
echo 'pitk\"{a}nen2019' | hobo unify
$ pitkanen2019
```
