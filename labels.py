"""
Search for citations in .tex files and output found labels to stdout.

The program accepts optional '\cite[a]{...}' as well as multiple labels, e.g.
'\cite{x, y, z}'.

Labels are sorted alphabetically.
"""

import os
import re

from pathlib import Path

from util import err, read_from, read_stdin, UrlFileOpenError

#                       \cite    [group(1)] {group(2)}
re_cite = re.compile(r"\\cite(?:\[(.*)?\])?\{(.+?)\}")


def find_labels(s):
    """Find all cited labels in a .tex file.

    Args:
        s (str): .tex file as a string

    Returns:
        set: Set of used labels (empty if none found)
    """
    labels = set()
    for match in re_cite.finditer(s):
        for label in match.group(2).split(','): # getting labels from '{x,y,z}'
            labels.add(label.strip())
    return labels


def main(args):
    """Returns 0 (success) or 1 (failure)
    """

    stdin_str = read_stdin()

    if not stdin_str and not args.inputs:
        args.inputs.append(Path(os.getcwd()))

    texfiles = set()
    for inp in args.inputs:
        if Path(inp).is_dir():
            texfiles.update({f for f in Path(inp).glob('*.tex')})
        else:
            texfiles.add(inp)

    labels = set()
    if stdin_str:
        labels.update(find_labels(stdin_str))

    for f in texfiles:
        try:
            tex_str = read_from(f)
        except UrlFileOpenError as e:
            return err(e)
        labels.update(find_labels(tex_str))

    for label in sorted(labels):
        print(label)
