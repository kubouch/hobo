"""
Extract given labels from a database and generate a .bib file into stdout.
"""

import os
import sys

from pathlib import Path

from util import err, read_from, read_stdin


def is_label_dir(p):
    """Check whether a given path can be a valid label name.

    `p` has to be a directory not starting with '.'.

    Args:
        p (pathlib.Path): Path to a label directory candidate

    Returns:
        bool: Is or isn't a valid label
    """

    return (p.is_dir() and not p.name.startswith('.'))


def is_field(p):
    """Check whether a given path can be a valid field name.

    `p` has to be a file with lowercase letters only in filename.

    Args:
        p (pathlib.Path): Path to a tested file

    Returns:
        bool: Is or isn't a valid field name
    """

    return (p.is_file() and p.name.isalpha() and p.name.islower())


def prompt_choose(items, msg='Choose:\n'):
    """Prompt user to choose an item from a list of items.

    This prints the following, waiting for an input:

        <msg>
        1: items[0]
        2: items[1]
        ...

    Expected answer is the number of the item.

    Args:
        items (iterable): items to choose from.
        msg (str): Intro message to display.

    Returns:
        chosen item
    """

    opts = {}
    for num, item in enumerate(items, 1):
        opts[str(num)] = item
        msg += "{}: {}\n".format(num, item)

    item = None
    while item is None:
        print(msg, file=sys.stderr)
        opt = input()
        item = opts.get(opt)
    print('', file=sys.stderr)
    sys.stderr.flush()
    return item


def find_db_labels(paths):
    """Find all labels in a list of databases.

    Args:
        paths (iterable over pathlib.Path): Iterable over valid database paths

    Returns:
        db_labels (dict): key - db path, value - list of labels in that db
    """

    db_labels = {}
    for dbd in paths:
        labels = [d.name for d in dbd.iterdir() if is_label_dir(d)]
        db_labels[dbd] = labels

    # Check for duplicates
    seen = []
    for dbd, labels in db_labels.items():
        seen.append(dbd)
        to_check = [k for k in db_labels.keys() if k not in seen]
        if to_check:
            for label in labels:
                conflicts = [dbd]
                for db_check in to_check:
                    if label in db_labels[db_check]:
                        conflicts.append(db_check)
                if len(conflicts) > 1:
                    msg = "Label `{}` is present in multiple databases."
                    msg += " Which do you want to choose?\n"
                    msg = msg.format(label)
                    chosen = prompt_choose(conflicts, msg)
                    for confl_db in conflicts:
                        if chosen != confl_db:
                            db_labels[confl_db].remove(label)
    return db_labels


def abbrev_first_names(value: str):
    """Abbreviate first names of an 'author' field
    """

    # some short-circuits
    if value.startswith('3rd'):
        return value

    full_names = []

    for full_name in value.split(' and '):
        if len(full_names) >= 6:
            full_names = [ full_names[0], 'others' ]
            break

        full_name = full_name.strip()
        if ',' in full_name:
            # surname, firstname format
            split = full_name.split(',')
            surname = split[0].strip()
            firstnames = split[1].strip()
        else:
            # firstname surname format
            split = full_name.split()
            surname = split[-1].strip()
            firstnames = ' '.join(split[:-1]).strip()

        if surname == 'others':
            full_names.append(surname)
            continue

        new_firstnames = []
        for firstname in firstnames.split():
            if len(firstname) == 1:
                print("WARNING: Possible missing dot in {}".format(value), file=sys.stderr)

            if '-' in firstname:
                # correctly abbreaviate Name1-Name2 style names
                new_firstname = []
                for partname in firstname.split('-'):
                    if len(partname) == 1:
                        print("WARNING: Possible missing dot in {}".format(value), file=sys.stderr)

                    if partname.endswith('.'):
                        new_firstname.append(partname)
                    else:
                        new_firstname.append(partname[0] + '.')

                new_firstnames.append('-'.join(new_firstname))
            else:
                if firstname.endswith('.'):
                    new_firstnames.append(firstname)
                else:
                    new_firstnames.append(firstname[0] + '.')

        full_names.append(' '.join(new_firstnames + [surname]))

    # print("result: '{}'".format(' and '.join(full_names)))

    return ' and '.join(full_names)


def bib_from_db(dbp, label, sur=['{', '}'], only_fields=None,
                except_fields=None, abbrev_first=False):
    """Create BibTex a entry from a provided database path and label pair.

    Args:
        dbp (pathlib.Path): Path to the database
        label (str): Label to extract fields from
        sur (tuple/list): Characters/strings that should surround the entry
        only_fields (list): Ignore all field names except of these
        except_fields (list): Use all field names except of these

    Return:
        str: A BibTex entry as a string
    """

    pentry = dbp.joinpath(label)

    entry_d = {}
    ID = label
    ENTRYTYPE = None
    for f in sorted(pentry.iterdir()):
        if f.name.startswith('_'):
            ENTRYTYPE = f.name[1:]
        if is_field(f):
            entry_d[f.name] = open(f).read()

    field_names = set(entry_d.keys())
    if only_fields:
        field_names = field_names & set(only_fields)
    if except_fields:
        field_names = field_names - set(except_fields)

    bib_str = "@{0}{{{1},\n".format(ENTRYTYPE, ID)
    for name in sorted(field_names):
        value = entry_d[name]
        if abbrev_first and name == 'author':
            value = abbrev_first_names(value)
        bib_str += "  {} = {}{}{},\n".format(name, sur[0], value.strip(),
                                             sur[1])
    bib_str += "}\n"

    return bib_str


def main(args):
    """Returns 0 (success) or 1 (failure)
    """

    labels_todo = []
    if not sys.stdin.isatty():
        labels_todo = [line.strip() for line in sys.stdin]

    cwd = Path(os.getcwd())
    if not args.d:
        db_paths = [cwd]
    else:
        db_paths = [Path(d) for d in args.d if Path(d).is_dir()]
    for d in db_paths:
        if not d.exists():
            return err("Directory '{}' does not exist.".format(d))
        if not d.is_dir():
            db_paths.remove(d)  # skip files
    if any([not args.surround_str[0], not args.surround_str[1]]):
        return err("--surround-str option should have two non-empty "
                   "arguments. Given: {}".format(args.surround_str))

    sys.stdin = open("/dev/tty", mode="r")
    db_labels = find_db_labels(db_paths)
    sys.stdin.close()

    not_found = []
    for label_todo in labels_todo:
        found = False
        for db, labels in db_labels.items():
            if label_todo in labels:
                print(bib_from_db(db, label_todo, args.surround_str,
                                  args.only_fields, args.except_fields,
                                  args.abbrev_first))
                found = True
        if not found:
            not_found.append(label_todo)
    if not_found:
        print("Warning: Labels not found: {}".format(not_found),
              file=sys.stderr)
