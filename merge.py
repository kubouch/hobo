"""
Parse bib files and merge them into a database.
"""

import sys

from importlib import import_module
from pathlib import Path

import bibtexparser
from bibtexparser.bibtexexpression import BibtexExpression
from bibtexparser.bparser import BibTexParser

from util import err, read_from, read_stdin, UrlFileOpenError


def entry_fields(pdir):
    """Iterate through files that can be entry fields.

    Valid entry field file name is
        a) all lowercase
        b) optionally starting with _

    Directories and files such as .gitkeep, README, etc. are ignored.

    Args:
        pdir (pathlib.Path): Path to the entry directory

    Yields:
        Files conforming to the above as Path objects
    """

    for p in pdir.iterdir():
        if all([p.is_file(), p.name.islower(),
               any([p.name[0].isalpha(), p.name.startswith('_')])]):
            yield p


def merge(bibdb, d, replace=False, labelfunc=None):
    """Merge a bibtex database into a directory.

    Args:
        bibdb (bibtexparser.bibdatabase.BibDatabase): parsed bibtex database
        d (pathlib.Path): valid target directory path
        replace (bool): replace all fields of an existing entry with new fields
    """

    for entry in bibdb.entries:
        entry_exists = False
        if not labelfunc:
            label = entry['ID']
        else:
            label = labelfunc(entry)
        plabel = d.joinpath(label)
        try:
            plabel.mkdir()
        except FileExistsError:
            entry_exists = True
            if replace:
                print("replacing:", label)
                for pfield in entry_fields(plabel):
                    pfield.unlink()
            else:
                print("already exists:", label)
                continue

        etype = entry['ENTRYTYPE']
        petype = plabel.joinpath('_' + etype)
        petype.touch()

        for field in entry.keys():
            if field not in ['ID', 'ENTRYTYPE']:
                pfield = plabel.joinpath(field)
                with open(pfield, 'x') as wf:
                    wf.write(entry[field] + '\n')

        if not entry_exists:
            print(label, etype)


def get_labelfunc(path):
    """Get a label-generating function from a file at 'path'.

    * The file has to implement a function called `labelfunc`
    * `labelfunc` must accept 1 argument - the BibTex entry (usually obtained
      with bibtexparser)
    * `labelfunc` must return a str with the generated label
    """

    p = Path(path)
    sys.path.insert(0, str(p.parent))

    labelfunc_module = import_module(p.stem) # throws exception if not found

    try:
        return labelfunc_module.labelfunc
    except AttributeError:
        raise AttributeError("Module {} has no function named 'labelfunc'"
                             .format(labelfunc_module))


def main(args):
    """Returns 0 (success) or 1 (failure)"""

    dpath = Path(args.d[0])
    if not dpath.exists():
        if args.parents:
            dpath.mkdir(parents=True)
        else:
            return err("Directory '{}' does not exist.".format(args.d[0]))
    if not dpath.is_dir():
        return err("'{}' is not a directory.".format(args.d[0]))

    if args.labelfunc:
        labelfunc = get_labelfunc(args.labelfunc)
    else:
        labelfunc = None

    stdin_str = read_stdin()

    if not stdin_str and not args.i:
        return err("No input .bib file provided.")

    if stdin_str:
        print("### Merging from stdin")
        try:
            bibdb = bibtexparser.loads(stdin_str,
                                       BibTexParser(common_strings=True))
        except BibtexExpression.ParseException as e:
            return err(e)
        merge(bibdb, dpath, args.force, labelfunc)
        print()

    bibfiles = set(args.i)
    for inp in bibfiles:
        try:
            bib_str = read_from(inp)
        except UrlFileOpenError as e:
            return err(e)

        print("### Merging from", inp)
        try:
            bibdb = bibtexparser.loads(bib_str,
                                       BibTexParser(common_strings=True))
        except BibtexExpression.ParseException as e:
            return err(e)
        merge(bibdb, dpath, args.force, labelfunc)
        print()
