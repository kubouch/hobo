#!/usr/bin/env python3

"""
Convert any string (from stdin or files) to a unified format using only
lowercase ASCII letters, numbers and hyphens.

Useful for converting titles or author names into a unified style that can be
used e.g. as file names.
"""

from util import err, read_stdin


def unify(s):
    """Perform the string conversion."""

    s = s.strip().lower()

    # Remove these characters
    to_remove = '"' + "'([{}])*!%\\"
    s = ''.join([c for c in s if c not in to_remove])

    # Convert all remaining non-alphabetic or non-digit characters to hyphens
    for c in s:
        if not (c.isalpha() or c.isdecimal()):
            s = s.replace(c, '-')

    # Replace repeated hyphens with one hyphen (e.g. in case of ', ')
    while '--' in s:
        s = s.replace('--', '-')

    return s


def main(args):
    stdin_str = read_stdin()

    if not stdin_str and not args.i:
        return err("No input strings provided.")

    if stdin_str:
        print(unify(stdin_str))

    strfiles = set(args.i)
    for strfile in strfiles:
        with open(strfile, 'r') as rf:
            for line in rf:
                print(unify(line))
